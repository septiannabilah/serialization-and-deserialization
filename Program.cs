﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Serialize
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" Before Serialization ");

            Truck truck = new Truck("Volvo", 10, 850.7);
            Bus bus = new Bus("Scania", 6, 54);

            OutputObject(truck);
            OutputObject(bus);

            Console.WriteLine("\n After Serialization ");

            byte[] truckByte = Serialize(truck);
            byte[] busByte = Serialize(bus);

            Console.WriteLine("Truck : ");
            OutputByte(truckByte);

            Console.WriteLine("\n Bus : ");
            OutputByte(busByte);

            Console.WriteLine("\n After Deserialization ");

            Vehicle volvoTruck = Deserialize(truckByte);
            Vehicle scaniaBus = Deserialize(busByte);

            OutputObject(volvoTruck);
            OutputObject(scaniaBus);
            
        }

        static byte[] Serialize(Object _vehicle) {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream()) {
                bf.Serialize(ms, _vehicle);
                return ms.ToArray();
            }
        }

        static Vehicle Deserialize(byte[] _byteVehicle) {
            using (var memStream = new MemoryStream()) {
                var binForm = new BinaryFormatter();
                memStream.Write(_byteVehicle, 0, _byteVehicle.Length);
                memStream.Seek(0, SeekOrigin.Begin);

                var obj = binForm.Deserialize(memStream);
                return (Vehicle)obj;
            }
        }

        static void OutputObject(Vehicle _vehicle) {
            Console.WriteLine("Name : " + _vehicle.GetName(), ", Tire(s) : " + _vehicle.GetTire());
            _vehicle.VehicleSpec();
        }

        static void OutputByte(byte[] _vehicleByte) {
            Console.WriteLine("new byte[] { " + string.Join(", ", _vehicleByte) + " }");
        }

    }
}
