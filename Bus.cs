using System;

namespace Serialize{
    public class Bus : Vehicle {
        private int capacity;
        public Bus (string _name, int _tire, int _capacity) : base (_name, _tire){
            capacity = _capacity;
        }

        public override void VehicleSpec(){
            Console.WriteLine("Vehicle Type : Bus", " | Passenger Capacity : " + capacity);
        }
    }
}