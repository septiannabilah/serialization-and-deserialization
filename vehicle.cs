using System;

namespace Serialize{
    [Serializable]
    public class Vehicle {
        protected string name;
        protected int tire;

        public Vehicle(string _name, int _tire){
            name = _name;
            tire = _tire;
        }
        public string GetName(){
            return this.name;
        }
        public int GetTire(){
            return this.tire;
        }

        public virtual void VehicleSpec(){
            
        }
    }
}