using System;

namespace Serialize{
    public class Truck : Vehicle {
        private double cargo;

        public Truck(string _name, int _tire, double _cargo): base(_name, _tire){
            cargo = _cargo;
        }

        public override void VehicleSpec(){
            Console.WriteLine("Vehicle Type : Truck", " | Cargo Capacity : " + cargo);
        }
    }
}